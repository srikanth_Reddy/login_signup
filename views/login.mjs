
document.getElementById('pass').maxLength = '15';
document.getElementById('pass').minLength = '10';


let button = document.querySelector('form');
button.addEventListener('submit', (event) => {
    event.preventDefault();
    let passErr = document.getElementById('passErr');
    let idErr = document.getElementById('idErr');
    let pass = event.target.pass.value;
    let mail = event.target.mail.value;
    let pattern = new RegExp("(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)");
    if (pattern.test(pass)) {
        passErr.textContent = "";
        let data = { mail, pass };
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
        fetch('http://localhost:4000/api/login', options)
            .then((data) => data.json())
            .then((data) => {
                if (data.mail === 1 && data.pass === 1) {
                    window.location.href = "./welcome.html";
                } else if (data.mail === 1 && data.pass === 0) {
                    idErr.textContent = ""
                    passErr.textContent = "password not valid";
                } else {
                    idErr.textContent = "user name invalid"
                    passErr.textContent = "";
                }

            })

    } else {
        passErr.textContent = "Must include Upppercass,lowercase,number";
    }


})


