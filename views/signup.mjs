document.getElementById('pass').maxLength = '15';
document.getElementById('pass').minLength = '10';


let button = document.querySelector('form');
button.addEventListener('submit', (event) => {
    event.preventDefault();
    let status = document.getElementById('status');
    let pass = event.target.pass.value;
    let mail = event.target.mail.value;
    let pattern = new RegExp("(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)");
    if (pattern.test(pass)) {
        status.textContent = "";
        let data = { mail, pass };
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
        fetch('http://localhost:4000/api/signup', options)
            .then((data) => data.json())
            .then((data) => {
                return status.textContent = data.msg;
            })

    } else {
        passErr.textContent = "Must include Upppercass,lowercase,number";
    }


})


